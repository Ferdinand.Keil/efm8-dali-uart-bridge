/*
 * Dali.h
 *
 *  Created on: 14.01.2020
 *      Author: fkeil
 */

#ifndef INC_DALI_H_
#define INC_DALI_H_

void Dali_setup(void);
void Dali_loop(void);

#endif /* INC_DALI_H_ */

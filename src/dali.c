/*
 * Dali.c
 *
 *  Created on: 14.01.2020
 *      Author: fkeil
 */

#include <uart_1.h>
#include <SI_EFM8LB1_Register_Enums.h>
#include <INTRINS.H>
#include "SI_LUT.h"
#include "dali.h"
#include "dali_cust_cmds.h"

//-----------------------------------------------------------------------------
// DEBUG
//-----------------------------------------------------------------------------
//#define Dali_DEBUG

#ifdef Dali_DEBUG
SI_SBIT(DALI_DEBUG_PIN0, SFR_P1, 6); // P1.6 DALI_DEBUG_PIN0
SI_SBIT(DALI_DEBUG_PIN1, SFR_P1, 7); // P1.7 DALI_DEBUG_PIN1
#define Dali_DEBUG_pin0_Low  do { DALI_DEBUG_PIN0 = 0; } while(0)
#define Dali_DEBUG_pin0_High do { DALI_DEBUG_PIN0 = 1; } while(0)
#define Dali_DEBUG_pin0_Flip do { DALI_DEBUG_PIN0 = ~DALI_DEBUG_PIN0; } while(0)
#define Dali_DEBUG_pin1_Low  do { DALI_DEBUG_PIN1 = 0; } while(0)
#define Dali_DEBUG_pin1_High do { DALI_DEBUG_PIN1 = 1; } while(0)
#define Dali_DEBUG_pin1_Flip do { DALI_DEBUG_PIN1 = ~DALI_DEBUG_PIN1; } while(0)
#else
#define Dali_DEBUG_pin0_Low
#define Dali_DEBUG_pin0_High
#define Dali_DEBUG_pin0_Flip
#define Dali_DEBUG_pin1_Low
#define Dali_DEBUG_pin1_High
#define Dali_DEBUG_pin1_Flip
#endif

//-----------------------------------------------------------------------------
// Timeout handling
//-----------------------------------------------------------------------------
uint8_t Timeout_startTime;
uint8_t Timeout_happened;

#define Timeout_reset()	do { \
	Timeout_startTime = TMR3L; \
	Timeout_happened = 0; \
} while(0)

uint8_t Timeout_wait(uint8_t timeout) {
	uint8_t wait, now;
	now = TMR3L;
	wait = ((now - Timeout_startTime) < timeout);
	if (!wait) {
		Timeout_happened = 1;
	}
	return wait;
}

#define TIMEOUT_CLOCK_FREQ (10000UL)

#define Timeout_teToTicks(te) ((TIMEOUT_CLOCK_FREQ * te * 416.67/1000000UL) + 0.5)

#define Timeout_waitTe(te) (Timeout_wait(Timeout_teToTicks(te)))

//-----------------------------------------------------------------------------
// DALI System Failure timeout
//-----------------------------------------------------------------------------
// enter system failure state after RX has been active for 500 ms
#define DALI_SYSTEMFAILURE_TIME 500

uint16_t Dali_SystemFailure_startTime;

#define Dali_SystemFailure_reset()	do { \
	Dali_SystemFailure_startTime = (TMR3H<<8) | TMR3L; \
} while(0)

uint8_t Dali_SystemFailure_wait() {
	uint16_t now = (TMR3H<<8) | TMR3L;
	return ((now - Dali_SystemFailure_startTime) < DALI_SYSTEMFAILURE_TIME);
}

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
#define DALI_RX_MATCH P1MAT
#define DALI_RX_MATCH_HIGH P1MAT_B4__HIGH
#define DALI_RX_MASK P1MASK
#define DALI_RX_MASK_COMPARED P1MASK_B4__COMPARED

// Maximum length for a DALI frame according to standard.
#define DALI_FRAME_MAXLEN 2
// Length of the buffer for data incoming from the DALI
// interface (RX) or outgoing to the UART.
// Length is the same as the maximum length for a DALI
// frame, so that forward frames from other masters on
// the bus can be received.
#define DALI_RXBUFFER_LEN DALI_FRAME_MAXLEN
// Length of the buffer for data incoming from the UART
// or outgoing to the DALI interface (TX).
// Length is four bytes to support custom commands.
#define DALI_TXBUFFER_LEN 4

#define DALI_MAGIC_WORDS 0xFACADE
#define DALI_MAGIC_WORD(n) ((uint8_t) (DALI_MAGIC_WORDS >> (16-n*8)))

#define Dali_txActive() do { DALI_PIN_TX = 0; } while(0)
#define Dali_txIdle()   do { DALI_PIN_TX = 1; } while(0)

#define Dali_rxIsIdle() (DALI_PIN_RX)

#define Dali_gpio0Write(x) do { DALI_PIN_GPIO0 = x; } while(0)
#define Dali_gpio0Read() (DALI_PIN_GPIO0)
// reset GPIO0 to its default (= off) state
// For the Quad-DALI-USB Interface this would be high level!
#define Dali_gpio0Reset() Dali_gpio0Write(1)

#define Dali_busyPinSet() do { DALI_PIN_BUSY = 0; } while(0)
#define Dali_busyPinClear() do { DALI_PIN_BUSY = 1; } while(0)

enum {
	DALI_INTERRUPT_RISE,
	DALI_INTERRUPT_FALL
};

#ifndef DALI_DEBUG_UART_ERRORS
#define DALI_DEBUG_UART_ERRORS 0
#endif

// checks whether timer did overflow
#define Dali_timerOverflowCheck() (TMR2CN0 & TMR2CN0_TF2H__BMASK)

// clears overflow bit
#define Dali_timerOverflowClear() do { TMR2CN0 &= ~TMR2CN0_TF2H__BMASK; } while(0)

// clears overflow bit and reloads timer registers
#define Dali_timerReload() do { \
	Dali_timerOverflowClear(); \
	TMR2L = TMR2RLL; \
	TMR2H = TMR2RLH; \
} while(0)

// waits for one Te using timer
#define Dali_waitForOneTe() do { \
	while (!Dali_timerOverflowCheck()) \
		; \
	Dali_timerOverflowClear(); \
	while (!Dali_timerOverflowCheck()) \
		; \
	Dali_timerOverflowClear(); \
} while(0)

// wait for one half Te using timer
#define Dali_waitForHalfTe() do { \
	while (!Dali_timerOverflowCheck()) \
		; \
	Dali_timerOverflowClear(); \
} while(0)

// Enables the port match interrupt for the start bit detection
#define Dali_interruptEnable() do { EIE1 |= EIE1_EMAT__ENABLED; } while(0)

// Disables the port match interrupt
#define Dali_interruptDisable() do { EIE1 &= ~EIE1_EMAT__ENABLED; } while(0)

// Changes the polarity of the port match interrupt
#define Dali_interruptPolarity(polarity) do { \
	if (polarity == DALI_INTERRUPT_RISE) { \
		DALI_RX_MATCH &= ~DALI_RX_MATCH_HIGH; \
	} else { \
		DALI_RX_MATCH |= DALI_RX_MATCH_HIGH; \
	} \
} while(0)

#define Dali_interruptPolarityFlip() do { DALI_RX_MATCH ^= DALI_RX_MATCH_HIGH; } while(0)

// Enables the port matching for the input pin
#define Dali_compareEnable() do {DALI_RX_MASK |= DALI_RX_MASK_COMPARED; } while(0)

// Disables the port matching for the input pin
#define Dali_compareDisable() do { DALI_RX_MASK &= ~DALI_RX_MASK_COMPARED; } while(0)

// Resets the hardware to the listening state
// is used for both the listening and the settling state, therefore
// it is in a separate function
#define Dali_listen() do { \
	Dali_txIdle(); \
	Dali_timerOverflowClear(); \
	Dali_interruptPolarity(DALI_INTERRUPT_FALL); \
	Dali_compareEnable(); \
} while(0)

// Sends DALI data over UART to host (workaround for bug in UART1 lib)
#define Dali_uartSend() do { \
	uint8_t tmp; \
	tmp = SFRPAGE; \
	SFRPAGE = 0x20; \
	UART1_writeBuffer(Dali_rxBuffer, Dali_rxLen); \
	SFRPAGE = tmp; \
} while(0)

//-----------------------------------------------------------------------------
// Custom types
//-----------------------------------------------------------------------------
typedef enum {
	DALI_STATE_FAILURE,
	DALI_STATE_LISTEN,
	DALI_STATE_TX_START,
	DALI_STATE_TX_SEND,
	DALI_STATE_TX_STOP,
	DALI_STATE_RX_1ST_TRAN,
	DALI_STATE_RX_RECEIVE,
	DALI_STATE_SETTLING_TIME
} Dali_state_t;

//-----------------------------------------------------------------------------
// Variables
//-----------------------------------------------------------------------------
volatile Dali_state_t Dali_state;

volatile bit Dali_edgeTrigger;

SI_SBIT(DALI_PIN_TX, SFR_P1, 5);    // P1.5 DALI_PIN_TX
SI_SBIT(DALI_PIN_RX, SFR_P1, 4);    // P1.4 DALI_PIN_RX
SI_SBIT(DALI_PIN_BUSY, SFR_P0, 0);  // P0.0 DALI_PIN_BUSY
SI_SBIT(DALI_PIN_GPIO0, SFR_P0, 1); // P0.1 DALI_PIN_GPIO0

SI_SEGMENT_VARIABLE(Dali_rxBuffer[DALI_RXBUFFER_LEN], uint8_t, EFM8PDL_UART1_TX_BUFTYPE);
uint8_t Dali_rxLen;
uint8_t Dali_txBuffer[DALI_TXBUFFER_LEN];
uint8_t Dali_txLen;

SI_SEGMENT_VARIABLE(Dali_uartBuffer[1], uint8_t, EFM8PDL_UART1_RX_BUFTYPE);

//-----------------------------------------------------------------------------
// Dali custom command interpreter routine
//
// Interprets custom four-byte commands.
void Dali_custCmdInterpreter(void) {
	// four byte cmds are used internally...
	if (Dali_txLen == 4) {
		// ...but only if they contain the magic word
		if ((Dali_txBuffer[0] == DALI_MAGIC_WORD(0)) &&
				(Dali_txBuffer[1] == DALI_MAGIC_WORD(1)) &&
				(Dali_txBuffer[2] == DALI_MAGIC_WORD(2))) {
			switch (Dali_txBuffer[3]) {
			case DALI_CUSTCMD_FIRMWARE_VERSION:
				Dali_rxBuffer[0] = DALI_FIRMWARE_VERSION;
				Dali_rxLen = 1;
				Dali_uartSend();
				break;
			case DALI_CUSTCMD_IS_FAILURE_STATE:
				if (Dali_state == DALI_STATE_FAILURE) {
					Dali_rxBuffer[0] = 0xFF;
				} else {
					Dali_rxBuffer[0] = 0x00;
				}
				Dali_rxLen = 1;
				Dali_uartSend();
				break;
			case DALI_CUSTCMD_GPIO0_READ:
				if (Dali_gpio0Read()) {
					Dali_rxBuffer[0] = 0xFF;
				} else {
					Dali_rxBuffer[0] = 0x00;
				}
				Dali_rxLen = 1;
				Dali_uartSend();
				break;
			case DALI_CUSTCMD_GPIO0_WRITE_HIGH:
				Dali_gpio0Write(1);
				Dali_rxBuffer[0] = 0xFF;
				Dali_rxLen = 1;
				Dali_uartSend();
				break;
			case DALI_CUSTCMD_GPIO0_WRITE_LOW:
				Dali_gpio0Write(0);
				Dali_rxBuffer[0] = 0xFF;
				Dali_rxLen = 1;
				Dali_uartSend();
				break;
			}
			// empty data buffer
			Dali_txLen = 0;
		}
	}
	//

}

//-----------------------------------------------------------------------------
// Dali setup routine
//
// Should be called once in the main routine to initialize the DALI protocol
// stack.
void Dali_setup(void) {
	Dali_DEBUG_pin0_Low;
	//Dali_DEBUG_pin1_Low;

	Dali_gpio0Reset();
	Dali_busyPinClear();

	Dali_state = DALI_STATE_LISTEN;
	Dali_listen();

	// enable and configure UART1
	UART1_init(25000000, 115200, UART1_DATALEN_8, UART1_STOPLEN_SHORT,
			  UART1_FEATURE_DISABLE, UART1_PARITY_ODD, UART1_RX_ENABLE,
			  UART1_RX_CROSSBAR, UART1_MULTIPROC_DISABLE);
	UART1_initTxFifo(UART1_TXTHRSH_ZERO, UART1_TXFIFOINT_DISABLE);
	UART1_initRxFifo(UART1_RXTHRSH_ZERO, UART1_RXTIMEOUT_16, UART1_RXFIFOINT_ENABLE);
	UART1_readBuffer(Dali_uartBuffer, 1);
}

//-----------------------------------------------------------------------------
// Dali loop routine
//
// Needs to be called repeatedly in the main loop. Handles the DALI
// communication.
void Dali_loop(void) {
	volatile uint8_t i, j, last;

	switch (Dali_state) {
	case DALI_STATE_FAILURE:
		// fluctuations on the line should not trigger the interrupt
		Dali_compareDisable();
		// in accordance with the standard the failure state will only be reached
		// if the DALI line is active (=low level) for 500 ms, otherwise return
		// to the listening state
		Dali_SystemFailure_reset();
		while (Dali_SystemFailure_wait()) {
			if (Dali_rxIsIdle()) {
				// DALI line returned to idle state, so go back to listening
				Dali_listen();
				Dali_state = DALI_STATE_LISTEN;
				// return *directly* to the listening state, without executing the
				// instructions before the next while loop
				return;
			}
		}
		// 500 ms have passed, now the failure state is finally entered
		// nothing is happening on the bus, so clear DALI_BUSY pin
		Dali_busyPinClear();
		// GPIO0 is cleared (set to low) in case of failure
		Dali_gpio0Reset();
		last = 0;
		while (Dali_state == DALI_STATE_FAILURE) {
			// going back to listening has priority!
			if (Dali_rxIsIdle()) {
				// DALI line returned to idle state, so go back to listening
				Dali_listen();
				Dali_state = DALI_STATE_LISTEN;
				break;
			}
			// keep resetting timeout while there is no data
			if (!Dali_txLen) {
				Timeout_reset();
			} else {
				// if data came in, reset timeout
				if (Dali_txLen > last) {
					Timeout_reset();
					last = Dali_txLen;
				}
				// if timeout occurred start evaluating command
				if (!Timeout_waitTe(1)) { // wait for 1 Te
					Dali_custCmdInterpreter();
					// as the DALI line is permanently idle, regular data is discarded
					Dali_txLen = 0;
					last = 0;
				}
			}
		}
		break;
	case DALI_STATE_LISTEN:
		// clear DALI_BUSY pin while idle
		Dali_busyPinClear();
		// wait for things to happen
		last = 0;
		while (Dali_state == DALI_STATE_LISTEN) {
			// keep resetting timeout while there is no data
			if (!Dali_txLen) {
				Timeout_reset();
			} else {
				// if data came in, reset timeout
				if (Dali_txLen > last) {
					Timeout_reset();
					last = Dali_txLen;
				}
				// if timeout occurred start sending data
				if (!Timeout_waitTe(1)) { // wait for 1 Te
					Dali_custCmdInterpreter();
					last = 0;
					// if the data was no custom command go to transmit
					if (Dali_txLen) {
						Dali_state = DALI_STATE_TX_START;
						break;
					}
				}
			}
		}
		// set DALI_BUSY pin while handling the bus
		Dali_busyPinSet();
		break;
	case DALI_STATE_TX_START:
		// disable receiver
		Dali_compareDisable();
		// send 1st transition of start
		Dali_timerReload();
		Dali_txActive();
		Dali_waitForOneTe();
		// send 2nd transition of start
		Dali_txIdle();
		Dali_waitForOneTe();
		Dali_state = DALI_STATE_TX_SEND;
		/* no break */
	case DALI_STATE_TX_SEND:
		for (j = 0; j < Dali_txLen; j++) {
			for (i = 0; i < 8; i++) {
				// check MSB
				if (Dali_txBuffer[j] & 0x80) {
					// high bit _-
					Dali_txActive();
					Dali_waitForOneTe();
					Dali_txIdle();
				} else {
					// low bit -_
					Dali_txIdle();
					Dali_waitForOneTe();
					Dali_txActive();
				}
				Dali_waitForOneTe();
				Dali_txBuffer[j] <<= 1; // shift left to get next bit
			}
		}
		Dali_txLen = 0; // all data sent, reset txLen
		// after all bytes were sent move to stop state
		Dali_state = DALI_STATE_TX_STOP;
		/* no break */
	case DALI_STATE_TX_STOP:
		// stay idle for two bit times (four Te)
		Dali_txIdle();
		Dali_waitForOneTe();
		Dali_waitForOneTe();
		Dali_waitForOneTe();
		Dali_waitForOneTe();
		Dali_state = DALI_STATE_SETTLING_TIME;
		/* no break */
	case DALI_STATE_SETTLING_TIME:
		// re-enable listening for answer
		Dali_listen();
		// refrain from sending for 22 Te (settling time)
		Timeout_reset();
		// stop waiting if state is changed externally, otherwise wait for timeout
		while ((Dali_state == DALI_STATE_SETTLING_TIME) && Timeout_waitTe(22)) ;
		// if timeout occurred go back to listen state
		if (Timeout_happened) {
			Dali_state = DALI_STATE_LISTEN;
		}
		break;
	case DALI_STATE_RX_1ST_TRAN:
		// wait for 2nd transition or timeout
		Timeout_reset();
		// stop waiting if state is changed externally, otherwise wait for timeout
		while ((Dali_state == DALI_STATE_RX_1ST_TRAN) && Timeout_waitTe(4)) ;
		// if timeout occurred go to failure state
		if (Timeout_happened) {
			Dali_state = DALI_STATE_FAILURE;
			break;
		}
		/* no break */
	case DALI_STATE_RX_RECEIVE:
		Dali_edgeTrigger = 0;
		Dali_timerReload();
		Dali_waitForOneTe();
		Dali_waitForHalfTe();
		Dali_rxLen = 0;
		for (i = 0; i < DALI_FRAME_MAXLEN; i++) {
			Dali_rxBuffer[i] = 0; // reset current byte
			for (j = 0; j < 8; j++) {
				// check & save state of DALI bus
				last = Dali_rxIsIdle();
				// wait for one Te
				Dali_timerReload();
				Dali_waitForOneTe();
				// check bus state again and decode bit
				if (last && !Dali_rxIsIdle()) {
					// -_ = logic zero
					// do nothing as byte has been zeroed
				} else if (!last && Dali_rxIsIdle()) {
					// _- = logic one
					Dali_rxBuffer[i] |= 0x80>>j;
				} else {
					// illegal state!
					// this could be a stop bit
					break;
				}
				// wait for a bus transitions, but don't wait longer than approx. one Te
				Timeout_reset();
				while (!_testbit_(Dali_edgeTrigger) && Timeout_wait(4)) ;
				if (Timeout_happened) {
					// illegal state!
					goto DALI_STATE_RX_RECEIVE_failure;
					break;
				} else {
					// so far we have waited for 1.5 Te
					// by waiting for one more Te we are in the middle of the first half of
					// the next bit
					// |bit 1 |bit 2 |...
					// |___---|---___|---
					//  ^   ^   ^
					//  <1.5>   ^
					//  <- 2.5 ->
					Dali_timerReload();
					Dali_waitForOneTe();
				}
			}
			if (j == 8) {
				// complete byte was transmitted, byte will be kept
				Dali_rxLen++;
			} else if (j == 0) {
				// stop bit detected, end of frame
				goto DALI_STATE_RX_RECEIVE_success;
				break;
			} else {
				// invalid data in the middle of the byte
				goto DALI_STATE_RX_RECEIVE_failure;
				break;
			}
		}
	// the following goto statements are used to break out of the nested loop
	DALI_STATE_RX_RECEIVE_success:
		Dali_uartSend();
		Dali_listen();
		Dali_state = DALI_STATE_LISTEN;
		break;
	DALI_STATE_RX_RECEIVE_failure:
		Dali_rxLen = 0; // data is discarded
		Dali_state = DALI_STATE_FAILURE;
		break;
	}
}

//-----------------------------------------------------------------------------
// PMATCH ISR Interrupt routine
//-----------------------------------------------------------------------------
SI_INTERRUPT (PMATCH_ISR, PMATCH_IRQn) {
	switch (Dali_state) {
	case DALI_STATE_LISTEN:
	case DALI_STATE_SETTLING_TIME:
		// change polarity for 2nd transition
		Dali_interruptPolarity(DALI_INTERRUPT_RISE);
		Dali_state = DALI_STATE_RX_1ST_TRAN;
		break;
	case DALI_STATE_RX_1ST_TRAN:
		// disable PMATCH interrupt
		Dali_interruptPolarity(DALI_INTERRUPT_FALL);
		Dali_state = DALI_STATE_RX_RECEIVE;
		break;
	case DALI_STATE_RX_RECEIVE:
		Dali_interruptPolarityFlip();
		Dali_edgeTrigger = 1;
		break;
	}
}

//-----------------------------------------------------------------------------
// UART ISR Callbacks
//-----------------------------------------------------------------------------
void UART1_receiveCompleteCb(void) {
	// retrieve UART data if state is idle or failure, discard data if not
	switch (Dali_state) {
	case (DALI_STATE_FAILURE):
	case (DALI_STATE_LISTEN):
		// limit frame length to defined max
		if (Dali_txLen < DALI_TXBUFFER_LEN) {
			Dali_txBuffer[Dali_txLen] = Dali_uartBuffer[0];
			Dali_txLen++;
		}
		break;
	}
	UART1_readBuffer(Dali_uartBuffer, 1);
}

void UART1_transmitCompleteCb(void) {
	// nothing to see here...
	UART1FCN0 &= ~UART1FCN0_TFRQE__ENABLED;
}

#if DALI_DEBUG_UART_ERRORS
volatile uint8_t Dali_uartErrorCount;
#endif

void UART1_transferErrorCb(uint8_t error) {
#if DALI_DEBUG_UART_ERRORS
	if(error & UART1_PARITY_EF) {
	  Dali_uartErrorCount++;
	}

	if(error & UART1_RXOVR_EF) {
	  Dali_uartErrorCount++;
	}
#else
	// dummy assignment to avoid warning 'unreferenced local variable'
	// see: http://www.keil.com/support/docs/2622.htm
	error = error;
#endif
}
